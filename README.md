# ChaCha20_python

Python implementation of the ChaCha20 stream cipher

## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

[@pts](https://github.com/pts) – [https://github.com/pts/chacha20](https://github.com/pts/chacha20)

[@cathalgarvey](https://github.com/cathalgarvey) – [https://gist.github.com/cathalgarvey/0ce7dbae2aa9e3984adc](https://gist.github.com/cathalgarvey/0ce7dbae2aa9e3984adc)

[@chiiph](https://github.com/chiiph) – [https://gist.github.com/chiiph/6855750](https://gist.github.com/chiiph/6855750)


Distributed under the GNU GPLv3 license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/chacha20_python>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request